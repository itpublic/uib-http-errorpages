%define build_date %(date +"%Y%m%d")
%define build_time %(date +"%H%M")
Summary: UiB HTTP-errorpages 
Name: uib-http-errorpages
#Note: el6 and .el7 will be added by the buildscript in vagrant
Version: %{build_date}
Release: %{build_time}
#Release: 1
License: Private  
BuildArch: noarch
Group:        Productivity/Networking/Web/Servers
BuildRoot: /var/tmp/%{name}-buildroot

%description
UiB styled errorpages for Apache and Nginx

%prep
rm -rf %{_builddir}/git
mkdir -p %{_builddir}/git
cd %{_builddir}/git
pwd
#git clone git@git.uib.no:sys/uib-http-errorpages
git clone https://git.app.uib.no/itpublic/uib-http-errorpages.git
find . -name '.git' -type d -exec rm {} \; 
find . -name '.gitig*' -type f -exec rm {} \; 

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/var/www/uib-errorpages
cp -a %{_builddir}/git/uib-http-errorpages/uib-errorpages/* $RPM_BUILD_ROOT/var/www/uib-errorpages/

%clean

%post
%postun

%files
%defattr(-,root,root)
#%doc README TODO COPYING ChangeLog
/var/www/uib-errorpages

%changelog
#* Mon Jan 04 2016 Michael Eric Menk <mme045@uib.no>
