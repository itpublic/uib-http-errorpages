#!/bin/bash
if [ ! -f /etc/redhat-release ] ;then
  echo "Not RHEL or CentOs, exiting"
  exit
fi

if [ -d ${HOME} ] ;then
  if [ -f ${HOME}/.rpmmacros ] ;then
    echo file exist
    exit
  fi
  echo '%_topdir '${HOME}/packages > ${HOME}/.rpmmacros
  mkdir -vp ${HOME}/packages/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
fi

specRootDir="/vagrant/el7/uib-http-errorpages"
specFile="uib-http-errorpages.spec"

if [ ! -d ${specRootDir} ] ; then
  echo To create rpm-file, run: rpmbuild -bb FILE.spec
  echo "not vagrant, exiting"
  exit
fi

echo "[el-base-dev]
name=UiB base development repository
baseurl=https://apprepo.uib.no/yum/el-base-dev/\$releasever
enabled=1
gpgcheck=0" > /etc/yum.repos.d/el-base-dev.repo

echo installing
yum -q clean all
yum install -y -q epel-release vim git rpm-build

if [ ! -f /root/.ssh/config ]; then
	echo "Defaults    env_keep += \"SSH_AUTH_SOCK\"" >> /etc/sudoers
	mkdir -p /root/.ssh/
	echo "Host *
	StrictHostKeyChecking no" > /root/.ssh/config
fi

cd ${HOME}
echo "
#Cleaning out ${HOME}/packages/RPMS, so that only one copy exists
rm -rf ${HOME}/packages/RPMS; mkdir -p ${HOME}/packages/RPMS
cd ${specRootDir} || exit 1
rpmbuild -bb ${specFile} || exit 2
cd ${HOME}/packages/RPMS/noarch/
#Creating one file for RHEL6 and one for RHEL7
for i in *.noarch.rpm ;do 
  cp -a  \$i \$(echo \$i | sed 's:\.noarch\.rpm:.el7.noarch.rpm:g') 
  rm -f  \$i
done
cd ${HOME}
#Listing the files
find ${HOME}/packages/ -iname '*rpm'
echo To copy to dev-repo, run:
echo scp ${HOME}/packages/RPMS/noarch/\*.el7.noarch.rpm repo@repo.app.uib.no:yum/incoming/el-base-dev/7/
echo To copy to test-repo, run:
echo scp ${HOME}/packages/RPMS/noarch/\*.el7.noarch.rpm repo@repo.app.uib.no:yum/incoming/el-base-test/7/
echo To copy to test-repo, run:
echo scp ${HOME}/packages/RPMS/noarch/\*.el7.noarch.rpm repo@repo.app.uib.no:yum/incoming/el-base-prod/7/
" > mk_rpm.sh
chmod a+x mk_rpm.sh

echo " "
echo "run: ${HOME}/mk_rpm.sh to build rpm"

# We need to log out root for ssh config fix to work!
echo "" && echo " => log in as root again with sudo -i"
kill -HUP $PPID
