# Error-pages

The files contained in this repo are errorpages that are shown if the web app is
not able to deliver its own page.

The error pages must be mounted to the URL `/UiBerror/` in order for all the pages to work.

## Production

The `master` branch is production, and can at any time be cloned/reset on a production machine. For this reason this branch must always be "production ready". 

## Test

There is a site for testing new/updated errorpages at http://error-pages.test.uib.no. The files get pulled from the `beta` branch via `cron.hourly`.

## Content
General purpose error pages for all sites:
 * http404.html - HTTP 404 Not found
 * http410.html - HTTP 410 Gone!
 * http50x.html - HTTP 500,501,502,503

Site specific error pages are start with capital text string.

Examples:
* EW_*.html - www.uib.no
* FILER_*.html - *.filer.uib.no
* MUSEET_*.html - universitetsmuseet.no

Blocked access

There is a set of pages for blocking access/logon from the outside. The pages have the CSS/Images on shared.app.uib.no, so they can be used by the server without having `/UiBerror/`. This enables the page to be passed through a script without changing web server.

* UiB_http403-noaccess.html
* UiB_http403-nologin.html

## Example install

### Inside a docker container
```
# Copy inn UiB styled errorpages
RUN cd  /var/www && \
    git clone https://git.app.uib.no/itpublic/uib-http-errorpages.git && \
    rm -rf /var/www/uib-http-errorpages/.git && \
    ln -s /var/www/uib-http-errorpages/uib-errorpages /var/www/

```
