Denne katalogen inneholder feil-sider til web-servere.

nginx-katalogen inneholder statiske filer, uten Apache sine spesielle variabler. Disse kan både brukes av Apache og Nginx.

Filen nginx-conf.inc er en konfig-fil for å få bruke disse sidene i Nginx.
