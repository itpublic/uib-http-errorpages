error_page 404             /UiBerror/http404.html;
error_page 410             /UiBerror/http410.html;
error_page 500 502 503 504 /UiBerror/http50x.html;
location /UiBerror {
    root  /var/www/uib-errorpages/nginx;
}
